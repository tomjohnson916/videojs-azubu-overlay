var gulp    = require('gulp')
  , connect = require('gulp-connect')
  , del     = require('del')
  , opn     = require('opn');

gulp
  // ------------------------------------------------
  // Clean Tasks
  //
  .task('clean', function(cb) {
    // clean production
    logTask('clean', paths.clean, []);
    del('dist/', function(){
      cb(null);
    });

  })

  // ------------------------------------------------
  // Test Tasks
  //
  .task('jslint', function() {
    return gulp
      .src(paths.hintJS)
      .pipe(jshint('.jshintrc'))
      .pipe(jshint.reporter('default'))
      .pipe(jshint.reporter('fail'));
  })

  // ------------------------------------------------
  // Web Server, Open Browser
  //
  .task('webserver', ['startserver'], function() {
    opn('http://localhost:8080');
  })
  .task('startserver', function() {
    connect.server();
  })
  .task('openbrowser', function() {
    opn('http://localhost:8080');
  })

  // ------------------------------------------------
  // Combined Tasks
  //
  .task('test', ['jslint'], function() {

  })
  .task('default', ['webserver', 'openbrowser'], function() {

  });

// ------------------------------------------------
// Support Routines
//

function logTask(task, inComing, outGoing ){
  if(verboseFlag){
    gutil.log('Task: ', task);
    gutil.log('Read: ', inComing);
    gutil.log('Write: ', outGoing);
  }
}
