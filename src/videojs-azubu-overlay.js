(function (window, videojs, undefined) {
  console.log('overlay plugin loaded');
  'use strict';

  var OVERLAY_DEFAULTS = {
    name: 'Default Overlay',
    url: '//azubu.tv/favicon.ico',
    href: '//abuzu.tv/',
    rect: {
      x: 0,
      y: 0,
      width: 256,
      height: 256
    },
    container: {
      width: 960,
      height: 540
    }
  };

  // Define Overlay Component
  videojs.AzubuOverlay = videojs.Component.extend({
    init: function(player, options, index) {
      // create component
      videojs.Component.call(this, player, options, index);
      // setup event handlers
      player.on('firstplay', videojs.bind(this, this.show));
      player.on('fullscreenchange', function() {
        console.log('player fullscreenchange, need to rescale');
        // TODO: for all of the player overlays, scale
      });
      player.on('resize', videojs.bind(this, this.scaleToPlayer));
      // set initial behaviors
      this.setPosition(options.rect.x, options.rect.y);
      this.setSize(options.rect.width, options.rect.height);
      this.scaleToPlayer(player, options.container);
      // hide on onset so the overlays don't show over the poster
      if(player.paused()) {
        this.hide();
      }
      // end
    }
  });

  videojs.AzubuOverlay.prototype.width = function(value) {
    if(value) {
      throw Error('Use setSize method to set the size');
    } else {
      return this.el().style.width || null;
    }
  };

  videojs.AzubuOverlay.prototype.height = function(value) {
    if(value) {
      throw Error('Use setSize method to set the size');
    } else {
      return this.el().style.height || null;
    }
  };

  videojs.AzubuOverlay.prototype.scaleToPlayer = function() {
    var containerReference = this.options_.container;
    var playerReference = this.player_;

    console.log('Player:', playerReference.width(), playerReference.height());
    console.log('Ad Orig Container:', containerReference.width, containerReference.height);
    // Container is different than original player rescale and reposition needed.
    var widthPerc, heightPerc, newXpos, newYpos;
    // Determine new relative width
    if(playerReference.width() < containerReference.width) {
      // player increased in width
      widthPerc = containerReference.width/playerReference.width();
    } else {
      // player decreased in width
      widthPerc = playerReference.width()/containerReference.width;
    }
    // Determine new relative height
    if(playerReference.height() < containerReference.height) {
      // player increased in height
      heightPerc = containerReference.height/playerReference.height();
    } else {
      // player decreased in height
      heightPerc = playerReference.height()/containerReference.height;
    }
    // Set new dimensions
    this.setSize(this.options_.rect.width*widthPerc, this.options_.rect.height*heightPerc);
    // Set new position
    this.setPosition(this.options_.rect.x*widthPerc, this.options_.rect.y*heightPerc);
  };

  videojs.AzubuOverlay.prototype.setPosition = function(xPos, yPos) {
    this.el().style.left = xPos + 'px';
    this.el().style.top = yPos + 'px';
  };

  videojs.AzubuOverlay.prototype.setSize = function(width, height) {
    this.el().style.width = width + 'px';
    this.el().style.height = height + 'px';
  };

  videojs.AzubuOverlay.prototype.createEl = function() {
    var el_ = videojs.Component.prototype.createEl.call(this, 'div', {
      className: 'vjs-azubu-overlay',
      innerHTML: '<a href="' + this.options_.href + '"><img width="100%" height="100%" src="' + this.options_.url + '" /></a>'
    });

    el_.style.position = 'absolute';
    el_.style.overflow = 'hidden';

    return el_;
  };
  // End Definition

  // Begin Plugin Definition
  var AzubuOverlay = function(options) {
    var player, overlayContainer;

    overlayContainer = document.createElement('div');
    overlayContainer.className = 'vjs-azubu-overlay-container';
    player = this;
    player.overlayContainer = overlayContainer;
    player.el().insertBefore(overlayContainer, player.el().querySelector('.vjs-loading-spinner'));

    player.AZUBU = player.AZUBU || {};
    player.AZUBU.DEFAULT_OVERLAY = OVERLAY_DEFAULTS;
    player.AZUBU.overlays = {};

    player.addOverlay = function(overlayConfig) {
      var overlay = new videojs.AzubuOverlay(player, overlayConfig);
          overlay.id = overlay.id_;
          overlay.name = overlayConfig.name;
          overlay.config = overlayConfig;

      player.AZUBU.overlays[overlay.id_] = overlay;
      player.overlayContainer.appendChild(overlay.el());
      return overlay;
    };

    player.removeOverlay = function(id) {
      if(player.AZUBU.overlays[id]) {
        // TODO: remove DOM element
        console.log('trying to remove', id);
        console.log(player.AZUBU.overlays[id]);
        player.overlayContainer.removeChild(player.AZUBU.overlays[id].el());
        // remove from player collection
        delete player.AZUBU.overlays[id];
      }
    };

    player.setOverlayPositionById = function(id, x, y) {
      var overlay = player.AZUBU.overlays[id];
      if(overlay) {
        overlay.setPosition(x, y);
        return true;
      }
      return false;
    };

    player.setOverlayDimensionsById = function(id, width, height) {
      var overlay = player.AZUBU.overlays[id];
      if(overlay) {
        overlay.setSize(width, height);
        return true;
      }
      return false;
    };

    player.setOverlayNameById = function(id, name) {
      if(player.AZUBU.overlays[id]) {
        player.AZUBU.overlays[id].name = name;
        return true;
      }
      return false;
    };

    // Initialize
    // TODO: for options.overlays ... add each
    if(options && options.overlays) {
      for (var i in options.overlays) {
        player.addOverlay(options.overlays[i]);
      }
    }

  };
  // End Definition

  // Register Plugin
  videojs.plugin('azubuOverlay', AzubuOverlay);

})(window, window.videojs);
